#pragma once

#include <constraints/NonCopyable.h>
#include <constraints/NonMoveable.h>
#include <utils/Optional.h>

struct ComputationalGraphNode
{
	explicit ComputationalGraphNode(const std::string& label);
	Optional<uint32_t> computationDepth;
	std::vector<ComputationalGraphNode*> predecessors;
	std::vector<ComputationalGraphNode*> successors;
	std::string label;
};

struct ComputationalGraphConstructData
{
	std::vector<std::pair<std::string, std::string>> edgeLabels;
};

class ComputationalGraph
	: private NonCopyable
	, private NonMoveable
{
public:
	explicit ComputationalGraph(const std::string& graphName);
	~ComputationalGraph();
	void construct(const ComputationalGraphConstructData&);

	const std::vector<ComputationalGraphNode*>& getRoots() const;
	std::vector<ComputationalGraphNode*>& getRoots();

	ComputationalGraphNode* findNode(const std::string& label);
	const ComputationalGraphNode* findNode(const std::string& label) const;
	const std::vector<ComputationalGraphNode*> collectAllNodes() const;
	std::vector<ComputationalGraphNode*> collectAllNodes();

	const std::string& getName() const;

	std::map<uint32_t, std::vector<ComputationalGraphNode*>> calculateComputationDepthMap() const;

private:
	void checkRoots();
private:
	struct ComputationalGraphPrivate* _members = nullptr;
};