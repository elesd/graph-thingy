#include <stdafx.h>
#include <graph/ComputationalGraphReader.h>

#include <graph/ComputationalGraph.h>

struct ComputationalGraphReaderPrivate
{
	std::string fileName;
	std::string graphName;
	std::ifstream input;
	std::string lastError;
	ComputationalGraphConstructData data;
	explicit ComputationalGraphReaderPrivate(const std::string& fileName)
		: fileName(fileName)
	{ }
};

ComputationalGraphReader::ComputationalGraphReader(const std::string& filePath)
	: _members(new ComputationalGraphReaderPrivate(filePath))
{

}

ComputationalGraphReader::~ComputationalGraphReader()
{
	delete _members;
	_members = nullptr;
}

std::unique_ptr<ComputationalGraph> ComputationalGraphReader::parse()
{
	_members->input = std::ifstream(_members->fileName);
	if(!_members->input)
	{
		_members->lastError = "Cannot open file:";
		_members->lastError += _members->fileName;
		return nullptr;
	}

	readGraphName();
	readConstructionData();
	std::unique_ptr<ComputationalGraph> result(new ComputationalGraph(_members->graphName));
	result->construct(_members->data);
	return result;
}

bool ComputationalGraphReader::hasError() const
{
	return !_members->lastError.empty();
}

ComputationalGraphReader::operator bool() const
{
	return !hasError();
}

const std::string& ComputationalGraphReader::getLastError() const
{
	return _members->lastError;
}

void ComputationalGraphReader::readGraphName()
{
	std::string line;
	std::getline(_members->input, line);
	std::vector<char> nameBuffer(1024);
	int32_t parsed = sscanf(line.c_str(), "%s", nameBuffer.data());
	if(parsed != 1)
	{
		_members->lastError = "Graph name parsing error";
		return;
	}
	_members->graphName = nameBuffer.data();
}

void ComputationalGraphReader::readConstructionData()
{
	_members->data.edgeLabels.clear();
	while(_members->input)
	{
		std::string line;
		std::getline(_members->input, line);
		if(line.empty())
			continue;

		std::vector<char> fromLabelBuffer(1024);
		std::vector<char> toLabelBuffer(1024);

		int32_t parsed = sscanf(line.c_str(), "%c->%c", fromLabelBuffer.data(), toLabelBuffer.data());
		if(parsed != 2)
		{
			_members->lastError = "Edges parsing error at line: ";
			_members->lastError += line;
			return;
		}
		_members->data.edgeLabels.push_back(std::make_pair(fromLabelBuffer.data(), toLabelBuffer.data()));
	}
}