#include <stdafx.h>
#include <graph/ComputationalGraphBuilder.h>

#include <graph/ComputationalGraph.h>

#include <utils/DotUtils.h>

struct ComputationalGraphBuilderPrivate
{
	ComputationalGraph* graph;
	explicit ComputationalGraphBuilderPrivate(ComputationalGraph* graph)
		: graph(graph)
	{	}
};

ComputationalGraphBuilder::ComputationalGraphBuilder(ComputationalGraph* graph)
	: _members(new ComputationalGraphBuilderPrivate(graph))
{

}

ComputationalGraphBuilder::~ComputationalGraphBuilder()
{
	delete _members;
	_members = nullptr;
}

void ComputationalGraphBuilder::build()
{
	cleanupGraph();
	std::deque<ComputationalGraphNode*> openSet = createOpenSet();
	while(!openSet.empty())
	{
		ComputationalGraphNode* node = openSet.front();
		openSet.pop_front();
		/*
		Value is set only when all parents are known.
		Therefore if it has value no further calculation is necessary.
		*/
		if(node->computationDepth.hasValue())
			continue;

		Optional<uint32_t> computationDepth = calculateComputationLevel(node);
		if(computationDepth.hasValue())
		{
			node->computationDepth.setValue(computationDepth + 1);
			for(ComputationalGraphNode* child : node->successors)
			{
				openSet.push_back(child);
			}
		}
	}
}

void ComputationalGraphBuilder::cleanupGraph()
{
	std::vector<ComputationalGraphNode*> nodes = _members->graph->collectAllNodes();
	for(ComputationalGraphNode* node : nodes)
	{
		node->computationDepth.reset();
	}

	std::vector<ComputationalGraphNode*> roots = _members->graph->getRoots();
	for(ComputationalGraphNode* node : roots)
	{
		node->computationDepth.setValue(0);
	}
}

std::deque<ComputationalGraphNode*> ComputationalGraphBuilder::createOpenSet()
{
	std::vector<ComputationalGraphNode*> roots = _members->graph->getRoots();
	std::deque<ComputationalGraphNode*> openSet;
	for(ComputationalGraphNode* root : roots)
	{
		std::copy(root->successors.begin(), root->successors.end(), std::back_inserter(openSet));
	}
	return openSet;
}

Optional<uint32_t> ComputationalGraphBuilder::calculateComputationLevel(ComputationalGraphNode* node)
{
	uint32_t computationDepth = 0;
	/*
	Skip if there is an ancestor without calculated depth.
	Skipped nodes will have got value after the last ancestor's depth has value.
	*/
	bool skip = false;
	for(auto it = node->predecessors.begin(); it != node->predecessors.end() && skip == false; ++it)
	{
		ComputationalGraphNode* parent = *it;
		if(!parent->computationDepth.hasValue())
		{
			skip = true;
		}
		else if(computationDepth < parent->computationDepth)
		{
			computationDepth = parent->computationDepth;
		}
	}
	Optional<uint32_t> result;
	if(!skip)
	{
		result.setValue(computationDepth);
	}
	return result;
}