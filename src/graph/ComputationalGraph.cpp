#include <stdafx.h>
#include <graph/ComputationalGraph.h>

ComputationalGraphNode::ComputationalGraphNode(const std::string& label)
	:label(label)
{ }

struct ComputationalGraphPrivate
{
	std::vector<std::unique_ptr<ComputationalGraphNode>> nodeContainer;
	std::vector<ComputationalGraphNode*> roots;
	std::string name;

	explicit ComputationalGraphPrivate(const std::string& name)
		:name(name)
	{ }
};

ComputationalGraph::ComputationalGraph(const std::string& graphName)
	:_members(new ComputationalGraphPrivate(graphName))
{

}

ComputationalGraph::~ComputationalGraph()
{
	delete _members;
	_members = nullptr;
}

void ComputationalGraph::construct(const ComputationalGraphConstructData& data)
{
	for(const auto& edgeData : data.edgeLabels)
	{
		ComputationalGraphNode* from = findNode(edgeData.first);
		ComputationalGraphNode* to = findNode(edgeData.second);
		if(from == nullptr)
		{
			std::unique_ptr<ComputationalGraphNode> createdFrom(new ComputationalGraphNode(edgeData.first));
			from = createdFrom.get();
			_members->nodeContainer.push_back(std::move(createdFrom));
			_members->roots.push_back(from);
		}
		if(to == nullptr)
		{
			std::unique_ptr<ComputationalGraphNode> createdTo(new ComputationalGraphNode(edgeData.second));
			to = createdTo.get();
			_members->nodeContainer.push_back(std::move(createdTo));
		}
		to->predecessors.push_back(from);
		from->successors.push_back(to);
	}
	checkRoots();
}

const std::vector<ComputationalGraphNode*>& ComputationalGraph::getRoots() const
{
	return _members->roots;
}

std::vector<ComputationalGraphNode*>& ComputationalGraph::getRoots()
{
	return _members->roots;
}

ComputationalGraphNode* ComputationalGraph::findNode(const std::string& label)
{
	auto it = std::find_if(_members->nodeContainer.begin(), _members->nodeContainer.end(),
						   [label](const std::unique_ptr<ComputationalGraphNode>& node)->bool
	{
		return node->label == label;
	});
	return it != _members->nodeContainer.end() ? it->get() : nullptr;
}

const ComputationalGraphNode* ComputationalGraph::findNode(const std::string& label) const
{
	auto it = std::find_if(_members->nodeContainer.begin(), _members->nodeContainer.end(),
						   [label](const std::unique_ptr<ComputationalGraphNode>& node)->bool
	{
		return node->label == label;
	});
	return it != _members->nodeContainer.end() ? it->get() : nullptr;
}

const std::vector<ComputationalGraphNode*> ComputationalGraph::collectAllNodes() const
{
	std::vector<ComputationalGraphNode*> result;
	std::transform(_members->nodeContainer.begin(), _members->nodeContainer.end(),
				   std::back_inserter(result),
				   [](const std::unique_ptr<ComputationalGraphNode>& node)->ComputationalGraphNode* { return node.get(); });
	return result;
}

std::vector<ComputationalGraphNode*> ComputationalGraph::collectAllNodes()
{
	std::vector<ComputationalGraphNode*> result;
	std::transform(_members->nodeContainer.begin(), _members->nodeContainer.end(),
				   std::back_inserter(result),
				   [](const std::unique_ptr<ComputationalGraphNode>& node)->ComputationalGraphNode* { return node.get(); });
	return result;
}

const std::string& ComputationalGraph::getName() const
{
	return _members->name;
}

std::map<uint32_t, std::vector<ComputationalGraphNode*>> ComputationalGraph::calculateComputationDepthMap() const
{
	std::map<uint32_t, std::vector<ComputationalGraphNode*>> result;
	for(const std::unique_ptr<ComputationalGraphNode>& node : _members->nodeContainer)
	{
		assert(node->computationDepth.hasValue());
		result[node->computationDepth].push_back(node.get());
	}
	return result;
}

void ComputationalGraph::checkRoots()
{
	auto it = std::remove_if(_members->roots.begin(), _members->roots.end(),
							 [](const ComputationalGraphNode* node)->bool { return !node->predecessors.empty(); });
	_members->roots.erase(it, _members->roots.end());
}
