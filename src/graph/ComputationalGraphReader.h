#pragma once

#include <constraints/NonCopyable.h>
#include <constraints/NonMoveable.h>

class ComputationalGraph;
struct  ComputationalGraphConstructData;

class ComputationalGraphReader
	: private NonCopyable
	, private NonMoveable
{ 
public:
	explicit ComputationalGraphReader(const std::string& filePath);
	~ComputationalGraphReader();

	std::unique_ptr<ComputationalGraph> parse();
	const std::string& getLastError() const;
	bool hasError() const;
	operator bool() const;
private:
	void readGraphName();
	void readConstructionData();
private:
	struct ComputationalGraphReaderPrivate* _members = nullptr;
};