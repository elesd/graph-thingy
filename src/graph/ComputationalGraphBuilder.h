#pragma once

#include <constraints/NonCopyable.h>
#include <constraints/NonMoveable.h>

#include <utils/Optional.h>

class ComputationalGraph;
struct ComputationalGraphNode;

class ComputationalGraphBuilder
	: private NonCopyable
	, private NonMoveable
{
public:
	explicit ComputationalGraphBuilder(ComputationalGraph* graph);
	~ComputationalGraphBuilder();

	void build();
private:
	void cleanupGraph();
	std::deque<ComputationalGraphNode*> createOpenSet();
	Optional<uint32_t> calculateComputationLevel(ComputationalGraphNode* node);
private:
	struct ComputationalGraphBuilderPrivate* _members = nullptr;
};