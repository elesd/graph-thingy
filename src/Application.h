#pragma once

#include <constraints/NonCopyable.h>
#include <constraints/NonMoveable.h>

class Application
	: private NonCopyable
	, private NonMoveable
{
public:
	explicit Application();
	~Application();
	bool init(int32_t argc, const char** argv);
	void run();
private:
	bool initApp(int32_t argc, const char** argv);
	bool loadGraph();
	bool parseArgs(int32_t argc, const char** argv);
	void buildGraph();
	void printResult();
private:
	struct ApplicationPrivate* _members = nullptr;
};