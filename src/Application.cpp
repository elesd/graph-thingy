#include <stdafx.h>
#include <Application.h>

#include <graph/ComputationalGraph.h>
#include <graph/ComputationalGraphBuilder.h>
#include <graph/ComputationalGraphReader.h>

#include <utils/Log.h>
#include <utils/DotUtils.h>

struct ApplicationPrivate
{
	std::unique_ptr<ComputationalGraph> graph;
	bool dumpResultToDot = false;
	std::string inputFilePath;
};

Application::Application()
	:_members(new ApplicationPrivate())
{
}

Application::~Application()
{
	delete _members;
	_members = nullptr;
}

bool Application::init(int32_t argc, const char** argv)
{
	return initApp(argc, argv);
}

void Application::run()
{
	if(!loadGraph())
		return;

	buildGraph();
	printResult();
}

bool Application::initApp(int32_t argc, const char** argv)
{
	bool parseOk = parseArgs(argc, argv);
	if(!parseOk)
	{
		std::cerr << "Invalid or missing arguments:" << std::endl
			<< "\tUsage: " << argv[0] << " <file_name> [--log] [--dump]" << std::endl;
	}
	return parseOk;
}

bool Application::loadGraph()
{
	Log::debug("Loading file...");
	ComputationalGraphReader reader(_members->inputFilePath);
	_members->graph = reader.parse();
	if(!reader)
	{
		std::cerr << "Something wrong happend during file reading: " << std::endl
			<< reader.getLastError() << std::endl;
		return false;
	}
	return true;
}

bool Application::parseArgs(int32_t argc, const char** argv)
{
	if(argc == 1)
		return false;

	_members->inputFilePath = argv[1];
	for(uint32_t i = 1; i < argc; ++i)
	{
		std::string arg = argv[i];
		if(arg == "--log")
		{
			Log::enable();
		}
		else if(arg == "--dump")
		{
			_members->dumpResultToDot = true;
		}
	}
	return true;
}

void Application::buildGraph()
{
	ComputationalGraphBuilder builder(_members->graph.get());
	builder.build();
}

void Application::printResult()
{
	if(_members->dumpResultToDot)
	{
		Log::debug("Dump to dot ...");
		std::string fileName = _members->graph->getName() + ".dot";
		DotUtils::dumpToFile(_members->graph.get(), fileName);
	}

	std::cout << "Result: " << std::endl;
	std::map<uint32_t, std::vector<ComputationalGraphNode*>> depthMap = _members->graph->calculateComputationDepthMap();
	for(const auto& pair : depthMap)
	{
		std::cout << pair.first << ": ";
		for(ComputationalGraphNode* node : pair.second)
		{
			std::cout << node->label << ",";
		}
		std::cout << std::endl;
	}
}