#pragma once

struct NonMoveable
{
protected:
	NonMoveable() = default;
	// No public inheritance allowed
	~NonMoveable() {}
	
	NonMoveable(NonMoveable&&) = delete;
	NonMoveable& operator=(NonMoveable&&) = delete;
};