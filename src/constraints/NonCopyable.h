#pragma once

struct NonCopyable
{
protected:
	NonCopyable() = default;
	// No public inheritance allowed
	~NonCopyable() {}

	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;
};