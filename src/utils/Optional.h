#pragma once

template<typename T>
class Optional
{
public:
	Optional() = default;
	explicit Optional(const T& v);

	operator const T&() const;
	operator T();

	bool hasValue() const;
	void reset();
	void setValue(const T& v);
private:
	T _value;
	bool _hasValue = false;
};

#include <utils/Optional.hpp>