
template<typename T>
Optional<T>::Optional(const T& v)
	: _value(v)
	, _hasValue(true)
{
}

template<typename T>
Optional<T>::operator const T&() const
{
	return _value;
}

template<typename T>
Optional<T>::operator T()
{
	return _value;
}

template<typename T>
bool Optional<T>::hasValue() const
{
	return _hasValue;
}

template<typename T>
void Optional<T>::reset()
{
	_value = T();
	_hasValue = false;
}

template<typename T>
void Optional<T>::setValue(const T& v)
{
	_value = v;
	_hasValue = true;
}