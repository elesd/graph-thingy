#pragma once

class ComputationalGraph;

struct DotUtils
{
	static std::string dump(const ComputationalGraph* graph);
	static void dumpToFile(const ComputationalGraph* graph, const std::string& fileName);
};