#include <stdafx.h>
#include <utils/DotUtils.h>

#include <graph/ComputationalGraph.h>

std::string DotUtils::dump(const ComputationalGraph* graph)
{
	std::ostringstream os;
	std::vector<ComputationalGraphNode*> nodes = graph->collectAllNodes();
	os << "digraph " << graph->getName() << " {" << std::endl;

	for(const ComputationalGraphNode* node : nodes)
	{
		os << node->label << " [label=\"" << node->label << " : ";
		if(node->computationDepth.hasValue())
		{
			os << node->computationDepth;
		}
		else
		{
			os << "?";
		}
		os << "\"];" << std::endl;
	}

	for(const ComputationalGraphNode* node : nodes)
	{
		for(const ComputationalGraphNode* child : node->successors)
		{
			os << node->label << " -> " << child->label << ";" << std::endl;
		}
	}
	os << "}" << std::endl;
	return os.str();
}

void DotUtils::dumpToFile(const ComputationalGraph* graph, const std::string& fileName)
{
	std::ofstream os(fileName, std::ofstream::out);
	os << dump(graph);
}