#include <stdafx.h>
#include <utils/Log.h>

#include <stdarg.h>

bool Log::enabled = false;

void Log::debug(const char* str, ...)
{
	if(!enabled)
		return;

	size_t length = strlen(str);
	std::vector<char> data(length);
	va_list args;
	va_start(args, str);
	vsnprintf(data.data(), data.size(), str, args);
	std::cout << data.data() << std::endl;
}

void Log::enable()
{
	enabled = true;
}

void Log::disable()
{
	enabled = false;
}