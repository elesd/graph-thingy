#pragma once

class Log
{
public:
	static void debug(const char* format, ...);
	static void enable();
	static void disable();
private:
	static bool enabled;
};