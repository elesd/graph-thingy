#include <stdafx.h>

#include <Application.h>

int main(int argc, const char** argv)
{
	Application app;
	
	if(app.init(argc, argv))
	{
		app.run();
	}

	char waitChar;
	std::cout << "Press Enter to continue" << std::endl;
	std::cin.get();
	return 0;
}