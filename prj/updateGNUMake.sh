if [ ! -d "GnuMake" ]
then
	mkdir GnuMake
fi

echo "Generate"
echo "============"
cd GnuMake
cmake -G "Unix Makefiles"  ../cmake
cd ..
