@echo off

if not exist "msvc2015" mkdir msvc2015

echo Generate
echo =====================
cd msvc2015
cmake -G "Visual Studio 14 2015" ..\cmake 
cd ..
pause
