if [ ! -d "UnixCodeBlocks" ]
then
	mkdir UnixCodeBlocks
fi

echo "Generate"
echo "============"
cd UnixCodeBlocks
cmake -G "CodeBlocks - Unix Makefiles"  ../cmake
cd ..
